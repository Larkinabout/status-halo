# Status Halo

This module arranges status icons around the edge of a token, rather than putting them in columns along the side.  

![preview image](documentation/basic_preview_image.webp)

## Installation

You can install the module manually using the manifest link
https://gitlab.com/mxzf/status-halo/-/releases/permalink/latest/downloads/module.json

## Credits

The original code and underlying idea come from the [PF2e Dorako UI](https://foundryvtt.com/packages/pf2e-dorako-ui) module, by Dorako